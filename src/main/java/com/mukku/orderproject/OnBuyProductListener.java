/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mukku.orderproject;

/**
 *
 * @author acer
 */
public interface OnBuyProductListener {
    public void Buy(Product product, int amount);
}
